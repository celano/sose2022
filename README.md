# sose2022

This repository contains material related to the course "Einführung in linguistische Annotation und XML Technologien" (Sommer Semester 2022)

## Useful websites
https://www.w3.org/TR/xpath-functions-31 <br/>
https://www.w3.org/TR/xpath-datamodel-31/ <br/>
https://docs.basex.org/wiki/Table_of_Contents <br/>
